package io.pagefault.cctask.domain.commands

interface Command<out T> {
    fun execute(): T
}
