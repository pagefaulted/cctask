package io.pagefault.cctask.domain.commands

import io.pagefault.cctask.domain.datasource.TaskListProvider

class MarkTaskDone(val taskListName: String,
                   val taskId: Long,
                   val server: String,
                   val port: Int,
                   private val taskListProvider: TaskListProvider = TaskListProvider()): Command<Boolean> {


    override fun execute(): Boolean = taskListProvider.markTaskDone(taskListName, taskId, server, port)
}
