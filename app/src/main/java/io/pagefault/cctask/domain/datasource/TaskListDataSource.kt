package io.pagefault.cctask.domain.datasource

import io.pagefault.cctask.domain.model.TaskList

interface TaskListDataSource {

    fun requestTaskList(taskListName: String, server: String, port: Int): TaskList?
    fun markTaskDone(taskListName: String, taskId: Long, server: String, port: Int): Boolean
}