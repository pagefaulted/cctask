package io.pagefault.cctask.domain.model

import java.time.ZonedDateTime

data class TaskList(val name: String, val tasks: List<Task>, val email: String?) {

    val size: Int
        get() = tasks.size

    operator fun get(position: Int) = tasks[position]
}

data class Task(val name: String,
                val description: String,
                val id: Long,
                val dueDate: ZonedDateTime?,
                val done: Boolean,
                val recurrence: Recurrence)

enum class Recurrence {
    NEVER,
    DAILY,
    MONTHLY,
    YEARLY
}
