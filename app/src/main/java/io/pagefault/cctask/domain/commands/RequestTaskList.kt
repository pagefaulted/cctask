package io.pagefault.cctask.domain.commands

import io.pagefault.cctask.domain.datasource.TaskListProvider
import io.pagefault.cctask.domain.model.TaskList

class RequestTaskList(
        val taskListName: String,
        val server: String,
        val port: Int,
        private val taskListProvider: TaskListProvider = TaskListProvider()) :
        Command<TaskList?> {

    override fun execute(): TaskList {
        val res = taskListProvider.requestTaskList(taskListName, server, port)
        return res
    }
}