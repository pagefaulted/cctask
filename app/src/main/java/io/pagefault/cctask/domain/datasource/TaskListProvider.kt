package io.pagefault.cctask.domain.datasource

import io.pagefault.cctask.data.server.persistence.TaskListHttpRequester
import io.pagefault.cctask.domain.model.TaskList
import io.pagefault.cctask.extensions.firstResult

class TaskListProvider(private val sources: List<TaskListDataSource> = SOURCES) {

    companion object {
        val SOURCES by lazy { listOf( TaskListHttpRequester()) }
    }

    fun requestTaskList(taskListName: String, server: String, port: Int): TaskList =
            requestToSources {
                it.requestTaskList(taskListName, server, port)
    }

    fun markTaskDone(taskListName: String, taskId: Long, server: String, port: Int): Boolean =
            requestToSources {
                it.markTaskDone(taskListName, taskId, server, port)
    }

    private fun <T : Any> requestToSources(f: (TaskListDataSource) -> T?): T = sources.firstResult { f(it) }
}