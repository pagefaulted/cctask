package io.pagefault.cctask.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import io.pagefault.cctask.R
import io.pagefault.cctask.extensions.DelegatesExt
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.toolbar.*

class SettingsActivity : AppCompatActivity() {

    companion object {
        const val TASK_LIST = "Task List Name"
        const val DEFAULT_TASK_LIST = "None"

        const val SERVER = "Server"
        const val DEFAULT_SERVER = "127.0.0.1"

        const val PORT = "Port"
        const val DEFAULT_PORT = 6969
    }

    private var taskListName: String by DelegatesExt.preference(this, TASK_LIST, DEFAULT_TASK_LIST)
    private var serverName: String by DelegatesExt.preference(this, SERVER, DEFAULT_SERVER)
    private var portNum: Int by DelegatesExt.preference(this, PORT, DEFAULT_PORT)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        taskList.setText(taskListName)
        server.setText(serverName)
        port.setText(portNum.toString())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        taskListName = taskList.text.toString()
        serverName = server.text.toString()
        portNum = port.text.toString().toInt()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> false
    }
}
