package io.pagefault.cctask.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import io.pagefault.cctask.R
import io.pagefault.cctask.domain.model.Task
import io.pagefault.cctask.domain.model.TaskList
import io.pagefault.cctask.extensions.ctx
import kotlinx.android.synthetic.main.item_task.view.*
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class TaskListAdapter(private val taskList: TaskList, private val markDone: (Task) -> Unit):
        RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.item_task, parent, false)
        return ViewHolder(view, markDone)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTask(taskList[position])
    }

    override fun getItemCount() = taskList.size

    class ViewHolder(view: View, private val markDone: (Task) -> Unit): RecyclerView.ViewHolder(view) {

        private val dueDateFormat = DateTimeFormatter.ofPattern("HH:mm EEE d MMM, ''yy")

        fun bindTask(task: Task) {
            with(task) {
                Picasso.with(itemView.ctx).load(getIcon(task.dueDate)).into(itemView.icon)
                itemView.dueDate.text = getDueDateStr(dueDate)
                itemView.taskName.text = name
                itemView.icon.setOnClickListener { markDone(this) }
            }
        }

        private fun getIcon(dueDate: ZonedDateTime?): Int {
            return if(dueDate != null && ZonedDateTime.now().toInstant().isAfter(dueDate.toInstant())) {
                R.drawable.mark_done_expired
            } else {
                R.drawable.mark_done
            }
        }

        private fun getDueDateStr(dueDate: ZonedDateTime?): String {
            return if(dueDate != null) {
                dueDate.withZoneSameInstant(ZoneId.systemDefault()).format(dueDateFormat)
            }
            else { "" }
        }
    }
}
