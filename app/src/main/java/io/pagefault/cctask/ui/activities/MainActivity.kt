package io.pagefault.cctask.ui.activities

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.pagefault.cctask.R
import io.pagefault.cctask.data.server.persistence.PersistedTaskList
import io.pagefault.cctask.data.server.persistence.TaskListDbHelper
import io.pagefault.cctask.data.server.persistence.TaskListTable
import io.pagefault.cctask.domain.commands.MarkTaskDone
import io.pagefault.cctask.domain.commands.RequestTaskList
import io.pagefault.cctask.domain.model.TaskList
import io.pagefault.cctask.extensions.DelegatesExt
import io.pagefault.cctask.ui.adapters.TaskListAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.task_list_activity.*
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.db.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class MainActivity : AppCompatActivity() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var executingAddActivity: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        container.adapter = mSectionsPagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        tabs.setupWithViewPager(container)
        findViewById<ViewPager>(R.id.container).offscreenPageLimit = 6

        add.setOnClickListener {
            executingAddActivity = true
            startActivity(Intent(this, SettingsActivity::class.java))
        }

        remove.setOnClickListener {
            mSectionsPagerAdapter?.fragments?.filter { it.isVisibleToUser }
                    ?.forEach {
                        mSectionsPagerAdapter?.removeFragment(it)
                    }
        }

        if (savedInstanceState != null) {
            Log.i(javaClass.name, "Restoring state from bundle...")

            val fragsCount = savedInstanceState.getInt("fragsCount")
            for (i in 1..fragsCount) {
                val taskListName = savedInstanceState.getString("taskListName$i")
                val server = savedInstanceState.getString("server$i")
                val port = savedInstanceState.getInt("port$i")
                mSectionsPagerAdapter?.addFragment(taskListName, server, port)
            }
            Log.i(javaClass.name, "Restored $fragsCount fragments")
        } else {
            Log.i(javaClass.name, "Attempt to restore state from db...")

            TaskListDbHelper.instance.use {
                select(TaskListDbHelper.TABLE_NAME).exec {
                    val taskLists = parseList(classParser<PersistedTaskList>())
                    Log.i(javaClass.name, "Found ${taskLists.size} persisted task lists")
                    taskLists.forEach {
                        val taskListName = it.taskListName
                        val server = it.server
                        val port = it.port.toInt()
                        mSectionsPagerAdapter?.addFragment(taskListName, server, port)
                    }
                }
            }
        }
    }


    fun persistState() {
        TaskListDbHelper.instance.use {
            this.transaction {
                delete(TaskListDbHelper.TABLE_NAME)
                mSectionsPagerAdapter?.fragments?.forEach {
                    val vals = ContentValues()
                    vals.put(TaskListTable.NAME, it.taskListName)
                    vals.put(TaskListTable.SERVER, it.server)
                    vals.put(TaskListTable.PORT, it.port)
                    insert(TaskListDbHelper.TABLE_NAME, null, vals)
                }
            }
        }
    }

    override fun onStop() {
        persistState()
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (outState != null) {
            val fragsCount = mSectionsPagerAdapter?.count ?: 0
            outState.putInt("fragsCount", fragsCount)
            mSectionsPagerAdapter?.fragments?.zip((1 to fragsCount).toList())?.forEach {
                outState.putString("taskListName${it.second}", it.first.taskListName)
                outState.putString("server${it.second}", it.first.server)
                outState.putInt("port${it.second}", it.first.port ?: 0)
            }
        }

        super.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        if (executingAddActivity) {
            executingAddActivity = false
            mSectionsPagerAdapter?.addFragment(taskListName, server, port)
        }
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        val fragments: ArrayList<PlaceholderFragment> = arrayListOf()

        private val fragmentNames: ArrayList<String> = arrayListOf()

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return fragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentNames[position]
        }

        fun removeFragment(frag: PlaceholderFragment) {
            synchronized(this) {
                runOnUiThread {
                    if (fragmentNames.contains(frag.taskListName)) {
                        fragments.remove(frag)
                        fragmentNames.remove(frag.taskListName)
                        TaskListDbHelper.instance.use {
                            delete(TaskListDbHelper.TABLE_NAME, "${TaskListTable.NAME} = {tasklistname}", "tasklistname" to (frag.taskListName
                                    ?: ""))
                        }
                    }
                    restartApp()
                }
            }
        }

        private fun restartApp() {
            val intent = Intent(applicationContext, MainActivity::class.java)
            val mPendingIntentId = 33
            val mPendingIntent = PendingIntent.getActivity(applicationContext, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT)
            val mgr = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
            System.exit(0)
        }

        fun addFragment(taskListName: String, server: String, port: Int) {
            bg {
                synchronized(this) {
                    try {
                        RequestTaskList(taskListName, server, port).execute()
                    } catch (e: Exception) {
                        Log.i(this.javaClass.name, "Could not load task list with name $taskListName from server $server at port $port")
                        e.printStackTrace()
                    }
                    runOnUiThread {

                        if (!fragmentNames.contains(taskListName)) {
                            val newFrag = PlaceholderFragment.newInstance(taskListName, server, port)
                            fragments.add(newFrag)
                            fragmentNames.add(taskListName)
                            notifyDataSetChanged()
                            persistState()
                            startBackgroundAlarm()
                        }
                    }
                }
            }
        }

        private fun startBackgroundAlarm() {

            val name = "notification"
            val description = name
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("notification", name, importance)
            channel.description = description
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)

            val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val i = Intent("com.example.app.alarm.action.trigger")
            i.setClass(baseContext, AlarmChecker::class.java)

            val taskListCount = fragments.size
            Log.i("startBackgroudAlarm", "Setting up background alarms for $taskListCount tasklists")
            i.putExtra("taskListCount", taskListCount)
            fragments.zip((1..taskListCount).toList()).forEach {
                i.putExtra("taskListName${it.second}", it.first.taskListName)
                i.putExtra("server${it.second}", it.first.server)
                i.putExtra("port${it.second}", it.first.port)
            }

            val pi = PendingIntent.getBroadcast(baseContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)
            am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 2000, 1000 * 60 * 30, pi)
        }
    }

    class PlaceholderFragment : Fragment() {
        private var mRecyclerView: RecyclerView? = null
        private var mLayoutManager: RecyclerView.LayoutManager? = null
        var taskListName: String? = null
        var server: String? = null
        var port: Int? = null
        var isVisibleToUser = false

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {

            val frameLayout = inflater.inflate(R.layout.task_list_activity, container, false)
            mRecyclerView = frameLayout.findViewById<RecyclerView>(R.id.taskList) as RecyclerView
            mLayoutManager = LinearLayoutManager(this.activity)
            mRecyclerView?.layoutManager = mLayoutManager
            (mRecyclerView?.parent as ViewGroup).removeView(mRecyclerView)

            if (taskListName == null) {
                taskListName = arguments?.get("taskList") as String
                server = arguments?.get("server") as String
                port = arguments?.get("port") as Int
            }

            if (taskListName != null && server != null && port != null) {
                loadTaskList(taskListName ?: "", server ?: "", port ?: 0)
            }

            return mRecyclerView
        }

        private fun loadTaskList(taskListName: String, server: String, port: Int) = doAsync {
            val result = RequestTaskList(taskListName, server, port).execute()
            uiThread {
                updateUI(result, server, port)
            }
        }

        private fun updateUI(taskListPayload: TaskList, server: String, port: Int) {
            val filteredTaskList = taskListPayload.copy(tasks = taskListPayload.tasks.filter { !it.done })
            val adapter = TaskListAdapter(filteredTaskList) {
                markDone(taskListPayload.name, it.id, server, port)
            }
            taskList.adapter = adapter
        }

        private fun markDone(taskListName: String, taskId: Long, server: String, port: Int) = doAsync {
                MarkTaskDone(taskListName, taskId, server, port).execute()
                loadTaskList(taskListName, server, port)
        }

        companion object {
            fun newInstance(taskListName: String, server: String, port: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putString("taskList", taskListName)
                args.putString("server", server)
                args.putInt("port", port)
                fragment.arguments = args
                fragment.taskListName = taskListName
                fragment.server = server
                fragment.port = port
                return fragment
            }
        }

        override fun setUserVisibleHint(isVisibleToUser: Boolean) {
            super.setUserVisibleHint(isVisibleToUser)
            this.isVisibleToUser = isVisibleToUser
            if(isVisibleToUser) {
                if (taskListName != null && server != null && port != null) {
                    loadTaskList(taskListName ?: "", server ?: "", port ?: 0)
                }
            }
        }
    }

    private val taskListName: String by DelegatesExt.preference(this, SettingsActivity.TASK_LIST,
            SettingsActivity.DEFAULT_TASK_LIST)

    private val server: String by DelegatesExt.preference(this, SettingsActivity.SERVER,
            SettingsActivity.DEFAULT_SERVER)

    private val port: Int by DelegatesExt.preference(this, SettingsActivity.PORT,
            SettingsActivity.DEFAULT_PORT)
}
