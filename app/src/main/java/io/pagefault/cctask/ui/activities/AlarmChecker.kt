package io.pagefault.cctask.ui.activities

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.support.v4.app.NotificationCompat
import io.pagefault.cctask.R
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import io.pagefault.cctask.domain.commands.RequestTaskList
import org.jetbrains.anko.coroutines.experimental.bg
import java.time.ZonedDateTime


class AlarmChecker : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            val extra = intent?.extras
            val taskListsCount: Int? = extra?.getInt("taskListCount")
            bg {
                if (taskListsCount != null) {

                    (1..taskListsCount).forEach {
                        val taskListName = extra.getString("taskListName$it")
                        val server = extra.getString("server$it")
                        val port = extra.getInt("port$it")
                        if (taskListName != null && server != null && port != null) {
                            val tasklist = RequestTaskList(taskListName, server, port).execute()
                            val mNotificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                            val existingNotifications = mNotificationManager.activeNotifications
                            tasklist.tasks.forEach {
                                if (existingNotifications.none { notification -> notification.id == it.name.hashCode() }) {
                                    if (it.dueDate != null && !it.done) {
                                        if (it.dueDate.toInstant().isBefore(ZonedDateTime.now().plusMinutes(30).toInstant())) {

                                            Log.i("Alarm", "Raising notification for $it")

                                            val mBuilder = NotificationCompat.Builder(context, "notification")
                                                    .setSmallIcon(R.drawable.bell)
                                                    .setContentTitle(it.name)
                                                    .setContentText("Task was due on ${it.dueDate}")
                                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                                            val notificationManager = NotificationManagerCompat.from(context)

                                            notificationManager.notify(it.name.hashCode(), mBuilder.build())
                                        }
                                    }
                                } else {
                                    Log.i("Alarm", "Notification already raised for $it")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
