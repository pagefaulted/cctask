package io.pagefault.cctask.data.server.persistence

object TaskListTable {
    const val NAME = "name"
    const val SERVER = "server"
    const val PORT = "port"
}