package io.pagefault.cctask.data.server.persistence

data class PersistedTaskList(val taskListName: String, val server: String, val port: String)
