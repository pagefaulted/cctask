package io.pagefault.cctask.data.server.persistence

import com.ctask.protocol.util.ToProtoUtils.createGetTaskListCommandEnvelope
import com.ctask.protocol.util.ToProtoUtils.createMaskAsDoneCommandEnvelope
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.isSuccessful
import io.pagefault.cctask.domain.datasource.TaskListDataSource
import io.pagefault.cctask.domain.model.TaskList

class TaskListHttpRequester: TaskListDataSource {

    override fun requestTaskList(taskListName: String, server: String, port: Int): TaskList? {
        val rootEnvelope = createGetTaskListCommandEnvelope(taskListName)

        // TODO: wrap in API that hides away the proto conversions
        // Send request
        val result = Fuel.post("http://$server:$port/command").
                body(rootEnvelope.toByteArray()).
                response()

        // TODO: add error handling
        return ProtocolToDomainConverter.taskListProtoConverter(result.second.data)
    }

    override fun markTaskDone(taskListName: String, taskId: Long, server: String, port: Int): Boolean {
        val rootEnvelope = createMaskAsDoneCommandEnvelope(taskId, taskListName)

        val result = Fuel.post("http://$server:$port/command").
                body(rootEnvelope.toByteArray()).
                response()

        return result.second.isSuccessful
    }
}
