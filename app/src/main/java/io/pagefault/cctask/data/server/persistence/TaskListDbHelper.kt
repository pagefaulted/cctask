package io.pagefault.cctask.data.server.persistence

import android.database.sqlite.SQLiteDatabase
import io.pagefault.cctask.ui.App
import org.jetbrains.anko.db.*

class TaskListDbHelper: ManagedSQLiteOpenHelper(App.instance,
        TABLE_NAME,
        null,
        DB_VERSION) {

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(TABLE_NAME)
        onCreate(db)
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(TABLE_NAME,
                true,
                TaskListTable.NAME to TEXT + PRIMARY_KEY,
                TaskListTable.SERVER to TEXT,
                TaskListTable.PORT to TEXT)
    }

    companion object {
        const val TABLE_NAME = "TaskLists"
        const val DB_VERSION = 2
        val instance by lazy { TaskListDbHelper() }
    }
}

