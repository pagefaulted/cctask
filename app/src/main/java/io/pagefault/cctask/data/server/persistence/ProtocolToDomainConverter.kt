package io.pagefault.cctask.data.server.persistence

import com.ctask.protocol.Envelope
import com.ctask.protocol.data.Data
import com.ctask.protocol.response.Response
import io.pagefault.cctask.domain.model.Recurrence
import io.pagefault.cctask.domain.model.Task
import io.pagefault.cctask.domain.model.TaskList
import java.time.ZonedDateTime

/**
 * Conversion utilities from protocol messages to domain objects.
 */
object ProtocolToDomainConverter {

    fun taskListProtoConverter(bytes: ByteArray): TaskList? {
        val rootEnvelopeProto = Envelope.RootEnvelopeProto.parseFrom(bytes)
        val responseProto = Response.ResponseProto.parseFrom(rootEnvelopeProto.payload)
        val taskListProto = Data.TaskListProto.parseFrom(responseProto.payload)
        val tasksProto = taskListProto.tasksList
        val tasks = tasksProto.map {
            Task(it.name,
                    it.description,
                    it.id,
                    if (it.dueDate.isPresent) ZonedDateTime.parse(it.dueDate.dateTimeStr) else null,
                    it.done,
                    Recurrence.NEVER)
        }

        return TaskList(taskListProto.name, tasks.sortedBy { it.dueDate }, "")
    }
}