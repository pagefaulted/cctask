# cctask
Android app for the CTask backend (https://github.com/modsrm/ctask)

# Credit
This app is written following the advices and the structure from this really good book that introduces the reader to Kotlin for Android development: https://antonioleiva.com/kotlin-android-developers-book/
